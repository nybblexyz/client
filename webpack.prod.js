const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = merge(common, {
    mode: 'production',
    devtool: 'source-map',
    performance: {
        hints: false,
        maxEntrypointSize: 512000,
        maxAssetSize: 512000
    },
    devServer: {
        historyApiFallback: true
    },
    // module: {
    //     rules: [{
    //         test: /\.scss$/,
    //         use: [
    //             MiniCssExtractPlugin.loader,
    //             "css-loader",
    //             "sass-loader"
    //         ]
    //     }]
    // },
    module: {
        rules: [{
            test: /\.css$/,
            use: [{
                loader: "style-loader"
            }, {
                loader: "css-loader"
            }, {
                loader: "sass-loader"
            },]
        }]
    },
    plugins: [
        new CleanWebpackPlugin(['dist/css', 'dist/js']),
        new MiniCssExtractPlugin({
            filename: "css/[name].css"
        }),
        new HtmlWebpackPlugin({
            title: 'my-title',
            favicon: `src/assets/logo/favicon.ico`,
            inject: false,
            // template: require('html-webpack-template'),
            template: `src/index.html`,
            bodyHtmlSnippet: '<div id="app"></div>'
        })
    ]
});