import React, { Children } from 'react';

import './index.css'

class Section extends React.Component {

    constructor(props) {
        super(props);
        this.state = {

        }
    }

    render() {
        const { className, style } = this.props;
        return (
            <div className={`wrapper_content ${className}`} style={style}>
                {this.props.children}
            </div>
        )
    }
}

export default Section