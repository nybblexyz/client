import React from 'react';
import './grab-btn.css';

class Button extends React.Component {

    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(e) {
        const { handleOnClick } = this.props;
        handleOnClick && handleOnClick(e);
    }

    render() {
        const { props } = this;
        return (
            <button
                className={`grab ${props.className}`}
                onClick={this.handleClick}
            >
                {props.content}
            </button>
        );
    }
}

export default Button;