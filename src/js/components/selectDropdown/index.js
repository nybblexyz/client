import React from 'react';

class SelectDropown extends React.Component {

    constructor(props) {
        super(props);
        this.state = {

        }
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) {
        const { onChange } = this.props;
        onChange && onChange(e)
    }
    render() {
        const { props } = this
        return (
            <div className="group">
                <div className="row">
                    <div className="col-20">
                        <label htmlFor={props.name}>{props.label}</label>
                    </div>
                    <div className="col-80">
                        <select name='options' value={props.value} onChange={(e) => this.handleChange(e)}>
                            <option value="0">{props.selectLabel}</option>
                            {props.option.length && props.option.map((option, i) => (
                                <option key={i} value={option.value}>{option.label}</option>
                            ))}
                        </select>
                    </div>
                </div>
            </div>
        )
    }
}

export default SelectDropown;

