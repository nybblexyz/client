import React from 'react';
import PropTypes from 'prop-types';
import './index.css';
class InputField extends React.Component {
    constructor(props) {
        super(props);

        this.state = {

        };
    }

    handleChange(event) {
        const { onChange } = this.props;
        onChange && onChange(event);
    };

    render() {
        const { type = 'text', value, placeholder, name, label, required, autoComplete } = this.props;
        const inputType = type == 'textarea';
        return (
            <div className="group">
                <div className="row">
                    <div className="col-20">
                        <label htmlFor={name}>{label}</label>
                    </div>
                    <div className="col-80">
                        {!inputType ?
                            <input
                                id={name}
                                type={type}
                                required={required}
                                value={value}
                                name={name}
                                onChange={(e) => this.handleChange(e)}
                                autoComplete={autoComplete}
                            /> :
                            <textarea
                                id={name}
                                type={type}
                                required={required}
                                value={value}
                                name={name}
                                onChange={(e) => this.handleChange(e)}
                            />}
                    </div>
                </div>
            </div>
        );
    }
}

export default InputField;