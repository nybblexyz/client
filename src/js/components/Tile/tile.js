import React from 'react';
import './tile.css';
import Button from '../buttons/grab-offer/grab-btn';
import Image from '../../../assets/upto60.png';

class Tile extends React.Component {

    render() {
        const { props } = this;
        let classnm = ["tile"];
        switch (props.back) {
            case 1:
                classnm.push("nd1");
                break;
            case 2:
                classnm.push("nd2");
                break;
            case 3:
                classnm.push("nd3");
                break;
            case 4:
                classnm.push("of1");
                break;
            case 5:
                classnm.push("of2");
                break;
            case 6:
                classnm.push("of3");
                break;
            default:
                classnm = ["tile"];
        }
        return (
            <div className='tile_box'>
                <div key={props.id} className={classnm.join(" ")}>
                    <img className='tileImage' src={Image} />
                    <p className="titleHead">{props.head}</p>
                    <p className="titleContent">{props.content}</p>
                    <div className="titleButton">
                        <Button url={props.url} content={props.buttonContent} />
                    </div>
                </div>
            </div>
        );
    }
}

export default Tile;