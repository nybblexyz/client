import React from "react";
import DatePicker from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";
import './index.css';
// CSS Modules, react-datepicker-cssmodules.css
// import 'react-datepicker/dist/react-datepicker-cssmodules.css';

class ReactDatePicker extends React.Component {

    constructor(props) {
        super(props);
        this.state = {};
        this.handleChange = this.handleChange.bind(this);
    }


    handleChange(date) {
        const { onChange } = this.props;
        onChange && onChange(date)
    };

    render() {
        const { props } = this;
        return (
            <div className="group">
                <div className="row">
                    <div className="col-20">
                        <label htmlFor={props.name}>{props.label}</label>
                    </div>
                    <div className="col-80 customDatePickerWidth">
                        <DatePicker
                            dateFormat="dd/MM/yyyy"
                            selected={props.selected}
                            onChange={(date) => this.handleChange(date)}
                            popperClassName='rasta-stripes'
                        />
                    </div>
                </div>
            </div>
        );
    }
}

export default ReactDatePicker;