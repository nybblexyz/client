import React from 'react';
import _ from 'lodash';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Home from './containers/Home/index';
import Login from './containers/login';

import './App.css';

export default class App extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            activeTab: 'home',
        }
    }

    componentDidMount() {
        // debugger
        const config = JSON.parse(localStorage.getItem('config')) || {};
        let locationPathname = 'home';
        if (config.hasOwnProperty('activeNav')) {
            locationPathname = config.activeNav;
        }
        this.setState({
            activeTab: locationPathname
        })
    }

    render() {
        const { activeTab } = this.state;
        return (
            <Router>
                <Switch>
                    <Route
                        path="/register" component={() => <Login />}
                    />
                    <Route
                        path="/home" component={() => <Home />}
                    />
                    <Route
                        path="/" component={() => <Home />}
                    />
                </Switch>
            </Router>
        )
    }
}