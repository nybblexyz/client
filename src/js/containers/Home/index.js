import React from 'react';

import Header from '../Header';
import Section from '../../components/Section';
import Tile from '../../components/Tile/tile';
import Footer from '../Footer';
import InputField from '../../components/input';
import Button from '../../components/buttons/grab-offer/grab-btn';

import './index.css';
// import Image from '../../../assets/banner.png';

class Home extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            link: '',
            message: '',
            activeTabCategory: 'e-Marketing',
            endOfTileArry: 5
        }
        this.handleActiveNavElement = this.handleActiveNavElement.bind(this);
        this.handleClickButton = this.handleClickButton.bind(this);
    }

    handleActiveNavElement(key, value) {
        this.setState({
            activeTabCategory: key,
            endOfTileArry: value
        }, () => console.log('state', this.state))
    }

    handleChange(event) {
        const name = event.target.name;
        const value = event.target.value;
        this.setState({
            [name]: value
        }, () => console.log('state', this.state))
    }

    handleClickButton(e) {
        debugger
    }

    render() {
        const { props } = this;
        const tileArry = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
        const { name, link, message, activeTabCategory, endOfTileArry } = this.state;
        return (
            <div>
                <Header />
                <Section>
                    <div className='row form'>
                        <div className='col-50'>
                            <div className='col-80 form_left_side'>
                                <InputField
                                    type='text'
                                    value={name}
                                    name={'name'}
                                    label='Brand Name'
                                    onChange={(e) => this.handleChange(e)}
                                />
                                <InputField
                                    type='text'
                                    value={link}
                                    name={'link'}
                                    label='Paste Your Link Here'
                                    onChange={(e) => this.handleChange(e)}
                                />
                                <InputField
                                    type='textarea'
                                    value={message}
                                    name={'message'}
                                    label='Type Your Message here'
                                    onChange={(e) => this.handleChange(e)}
                                />
                                <div className="formButton">
                                    <Button onClick={(e) => this.handleClickButton(e)} content={'copy'} />
                                </div>
                            </div>
                        </div>
                        <div className='col-50 form_right_side'>
                            <div className='box'></div>
                        </div>
                    </div>

                    <div className='section-nav'>
                        <ul>
                            <li className={activeTabCategory == 'free' ? 'active' : ''} onClick={() => this.handleActiveNavElement('free', 2)}>Free</li>
                            <li className={activeTabCategory == 'e-Marketing' ? 'active' : ''} onClick={() => this.handleActiveNavElement('e-Marketing', 5)}>e-Marketing</li>
                            <li className={activeTabCategory == 'Promo' ? 'active' : ''} onClick={() => this.handleActiveNavElement('Promo', 4)}>Promo</li>
                            <li className={activeTabCategory == 'Discount' ? 'active' : ''} onClick={() => this.handleActiveNavElement('Discount', 1)}>Discount</li>
                            <li className={activeTabCategory == 'Other' ? 'active' : ''} onClick={() => this.handleActiveNavElement('Other', 7)}>Other</li>
                        </ul>
                    </div>

                    <div className='row tile_content'>
                        {tileArry.length && tileArry.slice(0, endOfTileArry).map((value) => (
                            <Tile
                                key={value}
                                id={value}
                                head={'Title'}
                                content={'Tile content HERE'}
                                buttonContent={'Select'}
                                url={'#'}
                            />
                        ))}
                    </div>
                </Section>
                <Footer />
            </div>
        )
    }
}

export default Home;