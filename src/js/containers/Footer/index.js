import React from 'react';
import './index.css';
class Footer extends React.Component {

    render() {
        return (
            <div className='row'>
                <div className='col-100'>
                    <footer id="footer">
                        <div className="row">
                            <div className="col-100">
                                <ul className="list-unstyled list-inline social text-center">
                                    <li className="list-inline-item"><a href="#"><i className="fa fa-facebook"></i></a></li>
                                    <li className="list-inline-item"><a href="#"><i className="fa fa-twitter"></i></a></li>
                                    <li className="list-inline-item"><a href="#"><i className="fa fa-instagram"></i></a></li>
                                    <li className="list-inline-item"><a href="#"><i className="fa fa-google-plus"></i></a></li>
                                    <li className="list-inline-item"><a href="#" target="_blank"><i className="fa fa-envelope"></i></a></li>
                                </ul>
                            </div>
                            <div className="col-100">
                                <div className="container">
                                    <p><u><a href="#/">company_Name</a></u> Gurugram, Haryana, IND</p>
                                    <p className="h6">© All right Reversed.<a className="text-green ml-2" href="#">company_Name</a></p>
                                </div>
                            </div>
                        </div>
                    </footer>
                </div>
            </div>
        )
    }
}

export default Footer;