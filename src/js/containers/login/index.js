import React from 'react';

import Header from '../Header';
import Footer from '../Footer';
import Section from '../../components/Section';
import InputField from '../../components/input';
import Button from '../../components/buttons/grab-offer/grab-btn';
import SelectDropdown from '../../components/selectDropdown';
import DatePicker from "../../components/datePicker";

import './index.css'

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            renderLoginScreen: false,
            userName: '',
            password: '',
            email: '',
            firstName: '',
            lastName: '',
            dob: new Date(),
            gender: '',
        }
        this.handleActiveNav = this.handleActiveNav.bind(this);
        this.handleSwitchTab = this.handleSwitchTab.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleSelectChange = this.handleSelectChange.bind(this);
    }

    handleActiveNav(key) {
        const { handleActiveNav } = this.props;
        handleActiveNav && handleActiveNav(key)
    }

    handleChange(event) {
        const name = event.target.name;
        const value = event.target.value;
        this.setState({
            [name]: value
        }, () => console.log('state', this.state))
    }

    handleSwitchTab(key) {
        this.setState({
            renderLoginScreen: key
        })
    }

    handleSelectChange(e) {
        this.setState({
            gender: e.target.value
        })
    }

    handleDateChange(date) {
        this.setState({
            dob: date
        });
    };

    handleSubmit() {
        const {
            userName,
            password,
            renderLoginScreen,
            email,
            firstName,
            lastName,
            dob,
            gender
        } = this.state;
        let postDataObj = {};
        if (renderLoginScreen) {

        } else {

        }
    }

    render() {
        const {
            props: {
            },
            state: {
                userName,
                password,
                renderLoginScreen,
                email,
                firstName,
                lastName,
                dob,
                gender
            }
        } = this;

        const genderOption = [{ label: 'Male', value: 'male' }, { label: 'Female', value: 'female' }, { label: 'Other', value: 'other' }]

        return (
            <div>
                <Header />
                <div className='row wrapper_content'>
                    <div className='col-100 login_signup_form'>
                        <div className='col-75 login_signup_form_content'>
                            <div className='login_signup_form_header'>
                                <Button className={`col-50 login_form_toggle_button ${renderLoginScreen ? 'active' : ''}`} handleOnClick={() => this.handleSwitchTab(true)} content={'Login Form'} />
                                <Button className={`col-50 sign_up_form_toggle_button ${!renderLoginScreen ? 'active' : ''}`} handleOnClick={() => this.handleSwitchTab(false)} content={'Registration Form'} />
                            </div>
                            <div className="container">
                                {renderLoginScreen ? (
                                    <div>
                                        <InputField
                                            type='text'
                                            value={userName}
                                            name={'userName'}
                                            label='Username'
                                            onChange={(e) => this.handleChange(e)}
                                        />
                                        <InputField
                                            type='password'
                                            value={password}
                                            name={'password'}
                                            label='Password'
                                            onChange={(e) => this.handleChange(e)}
                                        />
                                    </div>
                                ) : (
                                        <div>
                                            <InputField
                                                type='text'
                                                value={firstName}
                                                name={'firstName'}
                                                label='First Name'
                                                onChange={(e) => this.handleChange(e)}
                                            />
                                            <InputField
                                                type='text'
                                                value={lastName}
                                                name={'lastName'}
                                                label='Last Name'
                                                onChange={(e) => this.handleChange(e)}
                                            />
                                            <InputField
                                                type='text'
                                                value={email}
                                                name={'email'}
                                                label='Email'
                                                onChange={(e) => this.handleChange(e)}
                                            />
                                            <SelectDropdown
                                                name={'gender'}
                                                label='Gender'
                                                selectLabel='Select Gender'
                                                value={gender}
                                                option={genderOption}
                                                onChange={(e) => this.handleSelectChange(e)}
                                            />
                                            <DatePicker
                                                value={dob}
                                                name={'dob'}
                                                label='Date of Birth'
                                                selected={dob}
                                                onChange={(date) => this.handleDateChange(date)}
                                            />
                                            <InputField
                                                type='password'
                                                value={password}
                                                name={'password'}
                                                label='Password'
                                                onChange={(e) => this.handleChange(e)}
                                            />
                                        </div>
                                    )}
                                <div className="row">
                                    <div className="col-100 mt-3 login_signup_form_button">
                                        <Button className="col-25 right" onClick={() => this.handleSubmit()} content={'Submit'} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer />
            </div>
        )
    }
}

export default Login;