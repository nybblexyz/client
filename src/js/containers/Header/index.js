import React from 'react';
import { Link } from 'react-router-dom';

import './index.css';

class Header extends React.Component {
    constructor() {
        super()
        this.state = {}
        this.handleToggleNav = this.handleToggleNav.bind(this);
    }

    handleToggleNav() {
        const { toggleNavButton } = this.state;
        this.setState({
            toggleNavButton: !toggleNavButton
        })
        // var x = document.getElementById("myTopnav");
        // if (x.className === "topnav") {
        //     x.className += " responsive";
        // } else {
        //     x.className = "topnav";
        // }
    }

    render() {
        const { toggleNavButton } = this.state;
        return (
            <div>
                <div className='grad'></div>
                <div className={`topnav ${toggleNavButton ? `responsive` : ''}`} id="myTopnav">
                    <Link to='/home'>Home</Link>
                    <Link to='/register'>Register</Link>
                    <Link to='/app'>App</Link>
                    <Link to='/contact'>Contact</Link>
                    <a href="#" className="icon" onClick={this.handleToggleNav}>
                        <i className="fa fa-bars"></i>
                    </a>
                </div>
            </div>
        )
    }
}

export default Header;